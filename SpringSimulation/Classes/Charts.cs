﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SpringSimulation.Classes.Utils;
using Axis = OxyPlot.Wpf.Axis;

namespace SpringSimulation.Classes
{
    public class Charts : BindableObject
    {
        private int _maxPointsFunction = 100;
        public int MaxPointsFunction
        {
            get
            {
                return _maxPointsFunction;
            }
            set
            {
                _maxPointsFunction = value;
                RaisePropertyChanged();
            }
        }

        public PlotModel FGHChartModel
        {
            get { return _fghChartModel; }
            set
            {
                _fghChartModel = value;
                RaisePropertyChanged();
            }
        }

        public PlotModel WChartModel
        {
            get { return _wChartModel; }
            set
            {
                _wChartModel = value;
                RaisePropertyChanged();
            }
        }

        public PlotModel XVAChartModel
        {
            get { return _xvaChartModel; }
            set
            {
                _xvaChartModel = value;
                RaisePropertyChanged();
            }
        }

        public PlotModel SChartModel
        {
            get { return _sChartModel; }
            set
            {
                _sChartModel = value;
                RaisePropertyChanged();
            }
        }

        //private ObservableCollection<DataPoint> fFunctionData;
        //private ObservableCollection<DataPoint> gFunctionData;
        //private ObservableCollection<DataPoint> hFunctionData;

        //private ObservableCollection<DataPoint> wFunctionData;

        //private ObservableCollection<DataPoint> xFunctionData;
        //private ObservableCollection<DataPoint> vFunctionData;
        //private ObservableCollection<DataPoint> aFunctionData;

        private PlotModel _fghChartModel;
        private PlotModel _wChartModel;
        private PlotModel _xvaChartModel;
        private PlotModel _sChartModel;


        public Charts()
        {
            SetupFGHChart();
            SetupWChart();
            SetupXVAChart();
            SetupSChart();
        }

        public void InvalidateCharts()
        {
            FGHChartModel.InvalidatePlot(true);
            WChartModel.InvalidatePlot(true);
            XVAChartModel.InvalidatePlot(true);
            SChartModel.InvalidatePlot(true);
        }

        public void ClearCharts()
        {
            ((LineSeries)FGHChartModel.Series[0]).Points.Clear();
            ((LineSeries)FGHChartModel.Series[1]).Points.Clear();
            ((LineSeries)FGHChartModel.Series[2]).Points.Clear();

            ((LineSeries)WChartModel.Series[0]).Points.Clear();

            ((LineSeries)XVAChartModel.Series[0]).Points.Clear();
            ((LineSeries)XVAChartModel.Series[1]).Points.Clear();
            ((LineSeries)XVAChartModel.Series[2]).Points.Clear();

            ((LineSeries)SChartModel.Series[0]).Points.Clear();
        }

        private void AddDataPointRange(IList<DataPoint> collection, DataPoint point, int range, OxyPlot.Axes.Axis axis)
        {
            if (collection.Count > range)
                collection.RemoveAt(0);
            collection.Add(point);
            //SetMinimum(axis, point.Y);
            //SetMaximum(axis, point.Y);
        }

        private void SetMinimum(OxyPlot.Axes.Axis axis, double value)
        {
            if (value < axis.Minimum)
            {
                axis.Minimum = value * 1.5f;
                axis.Reset();
            }
        }

        private void SetMaximum(OxyPlot.Axes.Axis axis, double value)
        {
            if (value > axis.Maximum + 2)
            {
                axis.Maximum = value * 1.5f;
                axis.Reset();
            }
        }

        public void AddDataToFGHChart(DataPoint fValue, DataPoint gValue, DataPoint hValue)
        {
            AddDataPointRange(((LineSeries)FGHChartModel.Series[0]).Points, fValue, MaxPointsFunction, FGHChartModel.Axes[0]);
            AddDataPointRange(((LineSeries)FGHChartModel.Series[1]).Points, gValue, MaxPointsFunction, FGHChartModel.Axes[0]);
            AddDataPointRange(((LineSeries)FGHChartModel.Series[2]).Points, hValue, MaxPointsFunction, FGHChartModel.Axes[0]);
        }

        public void AddDataToWChart(DataPoint wValue)
        {
            AddDataPointRange(((LineSeries)WChartModel.Series[0]).Points, wValue, MaxPointsFunction, WChartModel.Axes[0]);
        }

        public void AddDataToXVAChart(DataPoint xValue, DataPoint vValue, DataPoint aValue)
        {
            AddDataPointRange(((LineSeries)XVAChartModel.Series[0]).Points, xValue, MaxPointsFunction, XVAChartModel.Axes[0]);
            AddDataPointRange(((LineSeries)XVAChartModel.Series[1]).Points, vValue, MaxPointsFunction, XVAChartModel.Axes[0]);
            AddDataPointRange(((LineSeries)XVAChartModel.Series[2]).Points, aValue, MaxPointsFunction, XVAChartModel.Axes[0]);
        }

        public void AddDataToSChart(DataPoint value)
        {
            AddDataPointRange(((LineSeries)SChartModel.Series[0]).Points, value, MaxPointsFunction, SChartModel.Axes[0]);
        }

        private void SetupSChart()
        {
            SChartModel = new PlotModel();

            SChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Bottom, Title = "v(t)", MajorGridlineStyle = LineStyle.None, MinorGridlineStyle = LineStyle.None, Minimum = -10, Maximum = 10 });
            SChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Left, Title = "a(t)", MajorGridlineStyle = LineStyle.None, MinorGridlineStyle = LineStyle.None, Minimum = -10, Maximum = 10 });

            SChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "v(a(t))" });

            SChartModel.LegendPosition = LegendPosition.BottomCenter;
            SChartModel.LegendPlacement = LegendPlacement.Outside;
            SChartModel.Title = "State space";
            SChartModel.TitleFontSize = 15;
        }

        private void SetupXVAChart()
        {
            XVAChartModel = new PlotModel();
            XVAChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Left, Title = "Value", MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.None, Minimum = -5, Maximum = 5 });
            XVAChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Bottom, Title = "Time", MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.None });

            XVAChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "x(t)" });
            XVAChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "v(t)" });
            XVAChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "a(t)" });

            XVAChartModel.LegendPosition = LegendPosition.RightTop;
            XVAChartModel.LegendPlacement = LegendPlacement.Outside;
            XVAChartModel.Title = "Spring related";
            XVAChartModel.TitleFontSize = 15;
        }

        private void SetupWChart()
        {
            WChartModel = new PlotModel();
            WChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Left, Title = "Value", MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.None, Minimum = -5, Maximum = 5 });
            WChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Bottom, Title = "Time", MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.None });

            WChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "w(t)" });

            WChartModel.LegendPosition = LegendPosition.RightTop;
            WChartModel.LegendPlacement = LegendPlacement.Outside;
            WChartModel.Title = "Spring base displacement";
            WChartModel.TitleFontSize = 15;
        }

        private void SetupFGHChart()
        {
            FGHChartModel = new PlotModel();
            FGHChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Left, Title = "Value", MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.None, Minimum = -5, Maximum = 5 });
            FGHChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Bottom, Title = "Time", MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.None });

            FGHChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "f(t)" });
            FGHChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "g(t)" });
            FGHChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "h(t)" });

            FGHChartModel.LegendPosition = LegendPosition.RightTop;
            FGHChartModel.LegendPlacement = LegendPlacement.Outside;
            FGHChartModel.Title = "Forces";
            FGHChartModel.TitleFontSize = 15;
        }
    }
}
