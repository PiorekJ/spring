﻿using System.Windows.Media;
using OpenTK;
using SpringSimulation.Classes.OpenTK;
using SpringSimulation.Classes.SceneObjects;

namespace SpringSimulation.Classes.Models
{
    public interface IColorable
    {
        Color Color { get; set; }
    }

    public abstract class Model: SceneObject
    {
        public IMesh Mesh;
        public Shader Shader;
        
        public abstract void Draw();
        public abstract void SetMesh();
        public abstract void SetShader();

        protected Model(Scene currScene) : base(currScene)
        {
        }
    }
}
