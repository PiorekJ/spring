﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using SpringSimulation.Classes.OpenTK;
using SpringSimulation.Classes.SceneObjects;
using SpringSimulation.Classes.Utils;

namespace SpringSimulation.Classes.Models
{
    public class ScenePlane : Model, IDisposable, IMovable, IColorable, ISelectable
    {
        private Color _color = Colors.LawnGreen;

        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                RaisePropertyChanged();
            }
        }

        public float PlaneWidth { get; set; }
        public float PlaneHeight { get; set; }

        public ScenePlane(float width, float height, Scene currScene) : base(currScene)
        {
            PlaneWidth = width;
            PlaneHeight = height;
            Shader = Shaders.BasicShader;
            Mesh = GeneratePlaneMesh(width, height);
        }

        public override void Draw()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Mesh.Draw();
        }

        public override void SetMesh()
        {
            Mesh = GeneratePlaneMesh(PlaneWidth, PlaneHeight);
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Translate(Vector3 newPosition)
        {
            Position = newPosition;
        }

        public void Rotate(Vector3 newRotation)
        {
            throw new NotImplementedException();
        }

        public void Scale(Vector3 newScale)
        {
            throw new NotImplementedException();
        }


        public void Select()
        {
            throw new NotImplementedException();
        }

        public void Deselect()
        {
            throw new NotImplementedException();
        }

        private IMesh GeneratePlaneMesh(float width, float height)
        {
            Mesh?.Dispose();
            List<VertexPC> vertices = new List<VertexPC>();
            vertices.Add(new VertexPC(Position.AddXYToVector3(-width / 2, height / 2), Color.ColorToVector3()));
            vertices.Add(new VertexPC(Position.AddXYToVector3(-width / 2, -height / 2), Color.ColorToVector3()));
            vertices.Add(new VertexPC(Position.AddXYToVector3(width / 2, -height / 2), Color.ColorToVector3()));
            vertices.Add(new VertexPC(Position.AddXYToVector3(width / 2, height / 2), Color.ColorToVector3()));

            List<uint> edges = new List<uint>() { 0, 1, 1, 2, 2, 3, 3, 0 };

            return new Mesh<VertexPC>(vertices, edges, MeshType.Lines, AccessType.Static);
        }
    }
}
