﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using SpringSimulation.Classes.OpenTK;
using SpringSimulation.Classes.SceneObjects;
using SpringSimulation.Classes.Utils;

namespace SpringSimulation.Classes.Models
{
    public class ScenePoint : Model, IMovable, IDisposable, ISelectable, IColorable
    {
        private Color _color = Colors.White;
        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                RaisePropertyChanged();
            }
        }

        public ScenePoint(Scene currScene) : base(currScene)
        {
            Shader = Shaders.BasicShader;
            Mesh = GeneratePointMesh();
        }

        public override void Draw()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Mesh.Draw();
        }

        public override void SetMesh()
        {
            Mesh = GeneratePointMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.BasicShader;
        }

        public void Translate(Vector3 newPosition)
        {
            throw new NotImplementedException();
        }

        public void Rotate(Vector3 newRotation)
        {
            throw new NotImplementedException();
        }

        public void Scale(Vector3 newScale)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            Mesh?.Dispose();
        }

        public void Select()
        {
            throw new NotImplementedException();
        }

        public void Deselect()
        {
            throw new NotImplementedException();
        }

        private IMesh GeneratePointMesh()
        {
            Mesh?.Dispose();
            return new Mesh<VertexPC>(new List<VertexPC>() { new VertexPC(Position, Color.ColorToVector3()) }, null, MeshType.Points, AccessType.Static);
        }
    }
}
