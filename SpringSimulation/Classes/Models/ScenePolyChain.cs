﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using SpringSimulation.Classes.OpenTK;
using SpringSimulation.Classes.SceneObjects;
using SpringSimulation.Classes.Utils;

namespace SpringSimulation.Classes.Models
{
    public class ScenePolyChain : Model, IDisposable, IMovable, IColorable, ISelectable
    {
        public Color Color { get; set; } = Colors.LawnGreen;

        private List<Vector3> _points;
        private VertexPC[] _drawPoints;

        public ScenePolyChain(Scene currScene) : base(currScene)
        {
            _points = new List<Vector3>();
            Shader = Shaders.PolyShader;
            Mesh = GenerateLineMesh();
        }

        public ScenePolyChain(List<Vector3> polyPoints, Scene currScene) : base(currScene)
        {
            _points = polyPoints;
            Shader = Shaders.PolyShader;
            Mesh = GenerateLineMesh();
        }

        public override void Draw()
        {
            for (int i = 0; i < _points.Count; i++)
            {
                _drawPoints[i] = new VertexPC(_points[i], Color.ColorToVector3());
            }
            ((Mesh<VertexPC>)Mesh).SetVertices(0, _drawPoints);
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Scene.SceneCamera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Scene.SceneCamera.GetProjectionMatrix());
            Mesh.Draw();
        }

        public override void SetMesh()
        {
            Mesh = GenerateLineMesh();
        }

        public override void SetShader()
        {
            Shader = Shaders.PolyShader;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Translate(Vector3 newPosition)
        {
            
        }

        public void TranslateEnd(Vector3 newPosition)
        {
            _points[_points.Count - 1] = newPosition;
        }

        public void TranslateBeginning(Vector3 newPosition)
        {
            _points[0] = newPosition;
        }

        public void Rotate(Vector3 newRotation)
        {
            throw new NotImplementedException();
        }

        public void Scale(Vector3 newScale)
        {
            throw new NotImplementedException();
        }

        public void Select()
        {
            throw new NotImplementedException();
        }

        public void Deselect()
        {
            throw new NotImplementedException();
        }

        private Mesh<VertexPC> GenerateLineMesh()
        {
            _drawPoints = new VertexPC[_points.Count];

            for (int i = 0; i < _points.Count; i++)
            {
                _drawPoints[i] = new VertexPC(_points[i], Color.ColorToVector3());
            }

            return new Mesh<VertexPC>(_drawPoints, null, MeshType.Lines, AccessType.Static);
        }
    }
}
