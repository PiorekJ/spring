﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
namespace SpringSimulation.Classes.OpenTK
{
    public interface IVertex
    {
        void Initialize();
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPC : IVertex
    {
        public Vector3 Position;
        public Vector3 Color;

        public static readonly int SizeInBytes = Marshal.SizeOf(typeof(VertexPC));
        public static readonly int PositionOffset = (int)Marshal.OffsetOf<VertexPC>(nameof(Position));
        public static readonly int ColorOffset = (int)Marshal.OffsetOf<VertexPC>(nameof(Color));

        public VertexPC(Vector3 position, Vector3 color)
        {
            Position = position;
            Color = color;
        }

        public VertexPC(float x, float y, float z, float sR, float sG, float sB)
        {
            Position = new Vector3(x,y,z);
            Color = new Vector3(sR,sG,sB);
        }

        public void Initialize()
        {
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3,VertexAttribPointerType.Float, false, SizeInBytes, PositionOffset);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, SizeInBytes, ColorOffset);
        }
    }
}
