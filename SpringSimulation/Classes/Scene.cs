﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using SpringSimulation.Classes.Models;
using SpringSimulation.Classes.SceneObjects;

namespace SpringSimulation.Classes
{
    public class Scene
    {
        public Camera SceneCamera;
        public List<Model> SceneModels;

        public Scene()
        {
            SceneCamera = new Camera(this);
            SceneModels = new List<Model>();
        }

        public ScenePoint AddPoint()
        {
            ScenePoint point = new ScenePoint(this);
            SceneModels.Add(point);
            return point;
        }

        public ScenePlane AddPlane(float width, float height)
        {
            ScenePlane plane = new ScenePlane(width, height, this);
            SceneModels.Add(plane);
            return plane;
        }

        public ScenePlane AddPlaneAt(Vector3 pos,float width, float height)
        {
            ScenePlane plane = new ScenePlane(width, height, this);
            plane.Position = pos;
            SceneModels.Add(plane);
            return plane;
        }

        public ScenePolyChain AddPolyChain(List<Vector3> polyPoints)
        {
            ScenePolyChain chain = new ScenePolyChain(polyPoints, this);
            SceneModels.Add(chain);
            return chain;
        }
    }
}
