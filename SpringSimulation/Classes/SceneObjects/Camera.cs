﻿using System.Windows;
using OpenTK;
using SpringSimulation.Classes.Utils;

namespace SpringSimulation.Classes.SceneObjects
{
    public class Camera : SceneObject, IMovable
    {
        public float FOV { get; set; } = Settings.DefaultFOV;
        public float ZNear { get; set; } = Settings.DefaultFOV;
        public float ZFar { get; set; } = Settings.DefaultZFar;

        private float _aspectRatio;

        public Matrix4 GetViewMatrix()
        {
            //Quaternion.FromEulerAngles(Rotation.Z,Rotation.Y, Rotation.X).Inverted()
            return Matrix4.CreateTranslation(-Position) * Matrix4.CreateFromQuaternion(Quaternion.Identity.Inverted());
        }

        public Matrix4 GetProjectionMatrix()
        {
            return Matrix4.CreatePerspectiveFieldOfView(FOV, _aspectRatio, ZNear, ZFar);
        }

        public void SetCurrentAspectRatio(float aspectRatio)
        {
            _aspectRatio = aspectRatio;
        }

        public void Translate(Vector3 newPosition)
        {

        }

        public void Rotate(Vector3 newRotation)
        {

        }

        public void Scale(Vector3 newScale)
        {

        }

        public Camera(Scene currScene) : base(currScene)
        {
            Position = Settings.DefaultCameraPosition;
        }
    }
}
