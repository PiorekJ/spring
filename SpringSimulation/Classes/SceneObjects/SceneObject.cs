﻿using OpenTK;
using SpringSimulation.Classes.Utils;

namespace SpringSimulation.Classes.SceneObjects
{
    public interface ISelectable
    {
        void Select();
        void Deselect();
    }

    public interface IMovable
    {
        void Translate(Vector3 newPosition);
        void Rotate(Vector3 newRotation);
        void Scale(Vector3 newScale);
    }

    public abstract class SceneObject : BindableObject
    {
        public Vector3 Position { get; set; } = Vector3.Zero;
        public Vector3 Rotation { get; set; } = Vector3.Zero;
        public Vector3 Scale { get; set; } = Vector3.One;

        public string ObjectName { get; set; }

        public Scene Scene;

        protected SceneObject(Scene currScene)
        {
            Scene = currScene;
        }

        public Matrix4 GetModelMatrix()
        {
            return Matrix4.CreateScale(Scale) * Matrix4.CreateFromQuaternion(Quaternion.FromEulerAngles(Rotation.Z, Rotation.Y, Rotation.X)) * Matrix4.CreateTranslation(Position);
        }
    }
}
