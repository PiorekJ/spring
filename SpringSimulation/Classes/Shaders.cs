﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics;
using SpringSimulation.Classes.OpenTK;

namespace SpringSimulation.Classes
{
    public static class Shaders
    {
        private static Shader _basicShader;
        public static Shader BasicShader
        {
            get
            {
                if (_basicShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Basic/BasicShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Basic/BasicShader.frag"))
                    {
                        _basicShader = new Shader(vert, frag);
                    }
                    return _basicShader;
                }
                return _basicShader;
            }
        }

        private static Shader _polyShader;
        public static Shader PolyShader
        {
            get
            {
                if (_polyShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/PolyChain/PolyShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/PolyChain/PolyShader.frag"),
                        geom = new GeometricShaderObject("./Shaders/PolyChain/PolyShader.geom"))

                    {
                        _polyShader = new Shader(vert, frag, geom);
                    }
                    return _polyShader;
                }
                return _polyShader;
            }
        }

    }
}
