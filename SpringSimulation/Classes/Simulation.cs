﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using OpenTK;
using OxyPlot;
using SpringSimulation.Classes.Models;
using SpringSimulation.Classes.Utils;

namespace SpringSimulation.Classes
{
    public class Simulation : BindableObject
    {
        public Scene Scene;

        public float SimulationDT
        {
            get { return _simulationDt; }
            set
            {
                _simulationDt = value;
                RaisePropertyChanged();
            }
        }

        public enum FunctionsEnum
        {
            Const,
            Signum,
            Sinus
        }

        public IEnumerable<FunctionsEnum> FunctionValues
        {
            get { return Enum.GetValues(typeof(FunctionsEnum)).Cast<FunctionsEnum>(); }
        }

        public FunctionsEnum SelectedHFunction
        {
            get { return _selectedHFunction; }
            set
            {
                _selectedHFunction = value;
                RaisePropertyChanged();
            }
        }

        public FunctionsEnum SelectedWFunction
        {
            get { return _selectedWFunction; }
            set
            {
                _selectedWFunction = value;
                RaisePropertyChanged();
            }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                RaisePropertyChanged();
            }
        }

        public bool IsPaused
        {
            get { return _isPaused; }
            set
            {
                _isPaused = value;
                RaisePropertyChanged();
            }
        }

        private float _currentFps;

        public float CurrentFps
        {
            get { return _currentFps; }
            set
            {
                _currentFps = value;
                RaisePropertyChanged();
            }
        }

        private int _chartUpdateStep = 25;

        public int ChartUpdateStep
        {
            get { return _chartUpdateStep; }
            set
            {
                _chartUpdateStep = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<DataPoint> ForceFPoints { get; set; }
        public IList<DataPoint> ForceGPoints { get; set; }
        public IList<DataPoint> ForceHPoints { get; set; }
        private ScenePlane Base;
        private ScenePlane Object;
        private ScenePolyChain Spring;

        public SimulationValues Values { get; set; }
        public Charts Charts { get; set; }

        public Simulation()
        {
            Scene = new Scene();
            Values = new SimulationValues();
            Charts = new Charts();
        }

        float objW = 0.5f;
        float objH = 0.5f;
        float sprintL = 3f;

        float baseW = 0.1f;
        float baseH = 2f;
        public void InitializeSimulation()
        {
            List<Vector3> points = new List<Vector3>() { new Vector3(-sprintL, 0, 0), new Vector3((float)Values.InitialPosition, 0, 0) };
            Spring = Scene.AddPolyChain(points);
            Object = Scene.AddPlane(objW, objH);
            Base = Scene.AddPlane(baseW, baseH);
            diffSquared = SimulationDT * SimulationDT;
            SetSimulationValues();
        }

        public void SetSimulationValues()
        {
            Object.Position = new Vector3((float)Values.InitialPosition, 0, 0);
            Base.Position = new Vector3(-sprintL, 0, 0);
            Spring.TranslateBeginning(Object.Position);
            Spring.TranslateEnd(Base.Position);
            currentPosition = Object.Position.X;
            previousPosition = currentPosition - Values.InitialVelocity * SimulationDT;
            t = 0;
            currMax = 0;
            velocity = Values.InitialVelocity;
        }

        private double velocity = 0;

        private float diffSquared;
        private double previousPosition;
        private double currentPosition;
        private bool _isPaused;
        private bool _isRunning;
        private FunctionsEnum _selectedHFunction;
        private FunctionsEnum _selectedWFunction;
        private double t = 0;

        private double CalculateMovementBasedOnFunction(FunctionsEnum function, double amplitude, double omega, double offset)
        {
            switch (function)
            {
                case FunctionsEnum.Const:
                    return amplitude;
                case FunctionsEnum.Signum:
                    return Math.Sign(amplitude * Math.Sin(omega * t + offset));
                case FunctionsEnum.Sinus:
                    return amplitude * Math.Sin(omega * t + offset);
                default:
                    return 0;
            }
        }

        private float currMax = 0;
        private int counter = 25;
        private float _simulationDt = 0.01f;

        public class ReturnValues
        {
            public double t;
            public double w;
            public double f;
            public double g;
            public double h;
            public double a;

            public double naext;
        }

      
        public void CalculateSpring()
        {
            t += SimulationDT;
            double w = CalculateMovementBasedOnFunction(SelectedWFunction, Values.AmplitudeW, Values.OmegaW, Values.OffsetW);
            double f = Values.Elasticity * (w - currentPosition);
            double g = Values.Dampening * velocity;
            double h = CalculateMovementBasedOnFunction(SelectedHFunction, Values.AmplitudeH, Values.OmegaH, Values.OffsetH);
            double a = (f - g + h) / Values.Mass;
            double nextPosition = diffSquared * a +
                                 2 * currentPosition - previousPosition;


            velocity = (nextPosition - currentPosition) / SimulationDT;

            previousPosition = currentPosition;
            currentPosition = nextPosition;

            Object.Translate(new Vector3((float)currentPosition, 0, 0));
            Base.Translate(new Vector3((float)w - sprintL, 0, 0));
            Spring.TranslateBeginning(Object.Position);
            Spring.TranslateEnd(Base.Position);

            counter++;
            if (counter > ChartUpdateStep)
            {
                Charts.AddDataToFGHChart(new DataPoint(t, f), new DataPoint(t, g), new DataPoint(t, h));
                Charts.AddDataToWChart(new DataPoint(t, w));
                Charts.AddDataToXVAChart(new DataPoint(t, currentPosition), new DataPoint(t, velocity), new DataPoint(t, a));
                Charts.AddDataToSChart(new DataPoint(velocity, a));
                counter = 0;
            }


        }
    }

    public class SimulationValues : BindableObject
    {
        private double _initialVelocity = 0;
        private double _initialPosition = 0;
        private double _dampening = 1;
        private double _elasticity = 1;
        private double _mass = 1;
        private double _amplitudeH = 0;
        private double _omegaH = 0;
        private double _offsetH = 0;
        private double _amplitudeW;
        private double _omegaW;
        private double _offsetW;

        public double InitialVelocity
        {
            get { return _initialVelocity; }
            set
            {
                _initialVelocity = value;
                RaisePropertyChanged();
            }
        }

        public double InitialPosition
        {
            get { return _initialPosition; }
            set
            {
                _initialPosition = value;
                RaisePropertyChanged();
            }
        }

        public double Dampening
        {
            get { return _dampening; }
            set
            {
                _dampening = value;
                RaisePropertyChanged();
            }
        }

        public double Elasticity
        {
            get { return _elasticity; }
            set
            {
                _elasticity = value;
                RaisePropertyChanged();
            }
        }

        public double Mass
        {
            get { return _mass; }
            set
            {
                _mass = value;
                RaisePropertyChanged();
            }
        }

        public double AmplitudeH
        {
            get { return _amplitudeH; }
            set
            {
                _amplitudeH = value;
                RaisePropertyChanged();
            }
        }

        public double OmegaH
        {
            get { return _omegaH; }
            set
            {
                _omegaH = value;
                RaisePropertyChanged();
            }
        }

        public double OffsetH
        {
            get { return _offsetH; }
            set
            {
                _offsetH = value;
                RaisePropertyChanged();
            }
        }

        public double AmplitudeW
        {
            get { return _amplitudeW; }
            set
            {
                _amplitudeW = value;
                RaisePropertyChanged();
            }
        }

        public double OmegaW
        {
            get { return _omegaW; }
            set
            {
                _omegaW = value;
                RaisePropertyChanged();
            }
        }

        public double OffsetW
        {
            get { return _offsetW; }
            set
            {
                _offsetW = value;
                RaisePropertyChanged();
            }
        }
    }
}

