﻿using System.Windows.Media;
using OpenTK;

namespace SpringSimulation.Classes.Utils
{
    public static class Extensions
    {
        public static Vector3 ColorToVector3(this Color color)
        {
            return new Vector3(color.ScR, color.ScG, color.ScB);
        }

        public static Vector3 AddXToVector3(this Vector3 vector, float X)
        {
            return new Vector3(vector.X + X, vector.Y, vector.Z);
        }

        public static Vector3 AddYToVector3(this Vector3 vector, float Y)
        {
            return new Vector3(vector.X, vector.Y + Y, vector.Z);
        }

        public static Vector3 AddZToVector3(this Vector3 vector, float Z)
        {
            return new Vector3(vector.X, vector.Y, vector.Z + Z);
        }

        public static Vector3 AddXYToVector3(this Vector3 vector, float X, float Y)
        {
            return new Vector3(vector.X + X, vector.Y + Y, vector.Z);
        }

        public static Vector3 AddXZToVector3(this Vector3 vector, float X, float Z)
        {
            return new Vector3(vector.X + X, vector.Y, vector.Z + Z);
        }

        public static Vector3 AddYZToVector3(this Vector3 vector, float Y, float Z)
        {
            return new Vector3(vector.X, vector.Y + Y, vector.Z + Z);
        }

        public static Vector3 AddXYZToVector3(this Vector3 vector, float X, float Y, float Z)
        {
            return new Vector3(vector.X + X, vector.Y + Y, vector.Z + Z);
        }
    }
}
