﻿using System.Diagnostics;

namespace SpringSimulation.Classes.Utils
{
    public class FPSCounter
    {
        private Stopwatch _stopwatch;
        private double dt;
        private int frames;


        public FPSCounter()
        {
            _stopwatch = new Stopwatch();
            dt = 0;
        }

        public double CountTimeFrame()
        {
            _stopwatch.Stop();
            dt = _stopwatch.Elapsed.TotalSeconds;
            _stopwatch.Reset();
            _stopwatch.Start();
            return dt;
        }

        private double timeFrame = 0.0f;
        int currFrames = 0;
        public int CountFPS(double timeBetweenFrames)
        {
            timeFrame += timeBetweenFrames;
            ++frames;
            currFrames = 0;
            if (timeFrame > 1.0f)
            {
                currFrames = (int)(frames / timeFrame);
                timeFrame -= 1.0f;
                frames = 0;
                return currFrames;
            }
            return 0;
        }

        public void Start()
        {
            _stopwatch.Start();
        }
    }
}
