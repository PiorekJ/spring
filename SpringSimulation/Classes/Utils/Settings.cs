﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace SpringSimulation.Classes.Utils
{
    public static class Settings
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0,0,10);
        public const float DefaultFOV = (float)Math.PI / 4;
        public const float DefaultZNear = 0.1f;
        public const float DefaultZFar = 50f;
    }
}
