﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OxyPlot.Axes;
using SpringSimulation.Classes;
using SpringSimulation.Classes.Utils;
using OxyPlot;
using OxyPlot.Series;

namespace SpringSimulation
{
    /// <summary>
    /// Interaction logic for ErrorCalculationWindow.xaml
    /// </summary>
    public partial class ErrorCalculationWindow : Window, INotifyPropertyChanged
    {
        public Simulation Simulation;

        public double DeltaTime
        {
            get
            {
                return _deltaTime;
            }
            set
            {
                _deltaTime = value;
                OnPropertyChanged();
            }
        }

        private double _deltaTime = 100;


        public double ApproxTime
        {
            get
            {
                return _approxTime;
            }

            set
            {
                _approxTime = value;
                OnPropertyChanged();
            }
        }

        private double _approxTime = 10;

        public PlotModel ChartModel
        {
            get
            {
                return _chartModel;
            }
            set
            {
                _chartModel = value;
                OnPropertyChanged();
            }
        }
        private PlotModel _chartModel;

        public PlotModel ResultModel
        {
            get
            {
                return _resultModel;
            }
            set
            {
                _resultModel = value;
                OnPropertyChanged();
            }
        }
        private PlotModel _resultModel;

        private List<double> _positionsSim;
        private List<double> _positionsCalc;

        private Timer _timer;
        private double _currTime;

        public ErrorCalculationWindow()
        {
            _positionsCalc = new List<double>();
            _positionsSim = new List<double>();

            InitializeComponent();
            SetupResultModel();

        }

        private double currentPosition;
        private double previousPosition;
        private double velocity;
        private double diffSquared;
        private bool _isCalcFinished;
        private bool IsCalcFinished
        {
            get { return _isCalcFinished; }
            set
            {
                _isCalcFinished = value;
                if (_isCalcFinished)
                    ErrorTextBoxBrush = _errorBrush;
                else
                    ErrorTextBoxBrush = _defaultBrush;

                OnPropertyChanged();

            }
        }
        private SolidColorBrush _errorTextBoxBrush;

        public SolidColorBrush ErrorTextBoxBrush
        {
            get { return _errorTextBoxBrush; }
            set
            {
                _errorTextBoxBrush = value;
                OnPropertyChanged();
            }
        }

        private bool _isResultChartVisible = false;
        public bool IsResultChartVisible
        {
            get
            {
                return _isResultChartVisible;
            }
            set
            {
                _isResultChartVisible = value;
                OnPropertyChanged();
            }
        }

        public double CurrMaxError
        {
            get
            {
                return _currMaxError;
            }
            set
            {
                _currMaxError = value;
                OnPropertyChanged();
            }
        }
        private double _currMaxError;

        private SolidColorBrush _errorBrush = new SolidColorBrush(Colors.Red);
        private SolidColorBrush _defaultBrush = new SolidColorBrush(Colors.White);

        public double CalculateForces()
        {
            double f = (-currentPosition);
            double a = f;
            double nextPosition = diffSquared * a + 2 * currentPosition - previousPosition;

            previousPosition = currentPosition;
            currentPosition = nextPosition;

            return currentPosition;
        }


        //t += SimulationDT;
        //double w = CalculateMovementBasedOnFunction(SelectedWFunction, Values.AmplitudeW, Values.OmegaW, Values.OffsetW);
        //double f = Values.Elasticity * (w - currentPosition);
        //double g = Values.Dampening * velocity;
        //double h = CalculateMovementBasedOnFunction(SelectedHFunction, Values.AmplitudeH, Values.OmegaH, Values.OffsetH);
        //double a = (f - g + h) / Values.Mass;
        //double nextPosition = diffSquared * a +
        //                      2 * currentPosition - previousPosition;


        //velocity = (nextPosition - currentPosition) / SimulationDT;

        //previousPosition = currentPosition;
        //currentPosition = nextPosition;

        private void CalculateButton_OnClick(object sender, RoutedEventArgs e)
        {
            IsCalcFinished = false;
            SetupChartModel();
            CurrMaxError = 0;
            currentPosition = 1;
            previousPosition = 1;
            diffSquared = 1 / DeltaTime * 1 / DeltaTime;
            _currTime = 0;
            _timer = new Timer();
            _timer.Interval = 1000 * 1 / (double)DeltaTime;
            _timer.Elapsed += OnElapsed;
            _timer.Enabled = true;
        }

        private void SetupResultModel()
        {
            ResultModel = new PlotModel();
            ResultModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Bottom, Title = "Delta Time", MajorGridlineStyle = LineStyle.None, MinorGridlineStyle = LineStyle.None, Minimum = 0.01, Maximum = 0.1 });
            ResultModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Left, Title = "Value", MajorGridlineStyle = LineStyle.None, MinorGridlineStyle = LineStyle.None, Minimum = 0.001, Maximum = 0.1 });
            ResultModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "E(dt)" });
            ResultModel.LegendPosition = LegendPosition.BottomCenter;
            ResultModel.LegendPlacement = LegendPlacement.Outside;
            ResultModel.Title = "Error results";
            ResultModel.TitleFontSize = 15;
            
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0100, 0.00503273));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0111, 0.00559604));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0125, 0.00630118));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0142, 0.00720982));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0166, 0.00842442));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0200, 0.01013131));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0250, 0.01270533));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0333, 0.01703178));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.0500, 0.05329339));
            ((LineSeries)ResultModel.Series[0]).Points.Add(new DataPoint(0.1, 0.02582562));

            ResultModel.InvalidatePlot(true);
        }

        private void SetupChartModel()
        {
            ChartModel = new PlotModel();
            ChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Bottom, Title = "Time", MajorGridlineStyle = LineStyle.None, MinorGridlineStyle = LineStyle.None, Minimum = 0, Maximum = ApproxTime });
            ChartModel.Axes.Add(new LinearAxis() { Position = AxisPosition.Left, Title = "Value", MajorGridlineStyle = LineStyle.None, MinorGridlineStyle = LineStyle.None, Minimum = -1.2, Maximum = 1.2 });
            ChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "sim(t)" });
            ChartModel.Series.Add(new LineSeries() { LineStyle = LineStyle.Solid, Title = "calc(t)" });
            ChartModel.LegendPosition = LegendPosition.BottomCenter;
            ChartModel.LegendPlacement = LegendPlacement.Outside;
            ChartModel.Title = "Error calculation";
            ChartModel.TitleFontSize = 15;
            ChartModel.InvalidatePlot(true);
        }

        private void OnElapsed(object sender, ElapsedEventArgs e)
        {
            _currTime += _timer.Interval / 1000;

            var sim = CalculateForces();
            var calc = Math.Cos(_currTime);


            var currError = Math.Abs(sim - calc);
            if (currError > CurrMaxError)
                CurrMaxError = currError;

            if (Math.Abs(_currTime - ApproxTime) < 0.0000001)
            {
                _timer.Enabled = false;
                IsCalcFinished = true;
            }

            ((LineSeries)ChartModel.Series[0]).Points.Add(new DataPoint(_currTime, sim));
            ((LineSeries)ChartModel.Series[1]).Points.Add(new DataPoint(_currTime, calc));
            ChartModel.InvalidatePlot(true);

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
