﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpringSimulation.Interfaces
{
    public interface IDisposable
    {
        bool Dispose();
    }
}
