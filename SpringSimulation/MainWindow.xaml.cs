﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenTK;
using SpringSimulation.Classes;
using OpenTK.Graphics.OpenGL;
using OxyPlot;
using OxyPlot.Axes;
using SpringSimulation.Classes.OpenTK;
using SpringSimulation.Classes.SceneObjects;
using SpringSimulation.Classes.Utils;
using Model = SpringSimulation.Classes.Models.Model;

namespace SpringSimulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }

        private FPSCounter FPSCounter;

        public MainWindow()
        {
            Simulation = new Simulation();
            FPSCounter = new FPSCounter();
            InitializeComponent();
        }

        private void GlDisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetCurrentAspectRatio(GlDisplayControl.AspectRatio);


            GL.Viewport(0, 0, GlDisplayControl.ControlWidth, GlDisplayControl.ControlHeigth);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
            GL.LineWidth(Settings.LineSize);

            Simulation.InitializeSimulation();
            CompositionTarget.Rendering += OnRender;
        }

        private void GlDisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.SceneCamera.SetCurrentAspectRatio(GlDisplayControl.AspectRatio);
            GL.Viewport(0, 0, GlDisplayControl.ControlWidth, GlDisplayControl.ControlHeigth);
        }

        private double physTimeFrame;
        private double chartTimeFrame;
        private int currentFPS;
        private int physUpdates;
        private double restFrame = 0;
        private void OnRender(object sender, EventArgs e)
        {
            physTimeFrame += FPSCounter.CountTimeFrame();
            //currentFPS = FPSCounter.CountFPS(physTimeFrame);
            //if (currentFPS != 0)
            //{
              //  Simulation.CurrentFps = currentFPS;
            //}

            GL.ClearColor(System.Drawing.Color.Black);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                //if (chartTimeFrame > 0.1f)
                //
                    //chartTimeFrame -= 0.1f;
                //}

            physUpdates = (int)(physTimeFrame / Simulation.SimulationDT);

            if (Simulation.IsRunning && !Simulation.IsPaused)
                for (int i = 0; i < physUpdates; i++)
                {
                    Simulation.CalculateSpring();
                }


            foreach (Model sceneObject in Simulation.Scene.SceneModels)
            {
                sceneObject.Draw();
            }

            if (Simulation.IsRunning)
                    Simulation.Charts.InvalidateCharts();

            physTimeFrame -= physUpdates * Simulation.SimulationDT;



            GlDisplayControl.SwapBuffers();
        }

        private void GlDisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            CompositionTarget.Rendering -= OnRender;
        }

        private void PauseButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (Simulation.IsRunning)
                Simulation.IsPaused = true;
        }

        private void StopButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (Simulation.IsRunning || Simulation.IsPaused)
            {
                Simulation.IsRunning = false;
                Simulation.IsPaused = false;
                Simulation.SetSimulationValues();
            }
        }

        private void StartButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (Simulation.IsPaused)
                Simulation.IsPaused = false;

            if (!Simulation.IsRunning)
            {
                Simulation.Charts.ClearCharts();
                Simulation.SetSimulationValues();
                Simulation.IsRunning = true;
            }

        }
    }

}
