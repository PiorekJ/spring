﻿#version 330 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec3 color;

out V_Out
{
	vec3 color;
} Out;

void main()
{
	Out.color = color;
	gl_Position = vec4(position,1);
}

